import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';
import { Category } from './category';
import { CategoryService } from './category.service';

@Component({
    selector: 'my-category-detail',
    templateUrl : 'app/category-detail.component.html'
})
export class CategoryDetailComponent implements OnInit {
    @Input() category : Category;
    @Output() close = new EventEmitter();
    error : any;
    navigated = false;
    
    constructor (
        private categoryService : CategoryService,
        private routerParams : RouteParams
    ){}
    
    ngOnInit() {
        if (this.routerParams.get('id') !== null){
            let id = this.routerParams.get('id');
            this.navigated = true;
            this.categoryService.getCategory(id)
                .then(category => this.category = category)
        }else {
            this.navigated = false;
            this.category = new Category();
        }
    }
    
    save() {
        this.categoryService
            .save(this.category)
            .then(category => {
                this.category = category;
                this.goBack(category);
            })
            .catch(error => this.error = error);
    }
    
    goBack(savedCategory : Category = null){
        this.close.emit(savedCategory);
        if (this.navigated){
            window.history.back();
        }
    }
}


