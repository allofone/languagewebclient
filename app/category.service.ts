import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt/angular2-jwt';

import 'rxjs/add/operator/toPromise';

import { Category } from './category';

@Injectable()
export class CategoryService {
    constructor(private authHttp : AuthHttp){}
    
    private token : string = localStorage.getItem('id_token');
    private categoriesUrl = `http://localhost:8000/api/categories`;
    
    getCategories() : Promise<Category[]> {        
        return this.authHttp.get(this.categoriesUrl)
            .toPromise()
            .then(response => response.json())            
            .catch(this.handleError);
    }
    
    getCategory(id : string){
        return this.getCategories()
            .then(categories => categories.filter(category => category.id === id)[0]);
    }
    
    save(category : Category){
        if (category.id){
            return this.put(category);
        }
        return this.post(category);
    }
    
    delete(category : Category){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let url = `${this.categoriesUrl}/${category.id}`;
        return this.authHttp
            .delete(url, headers)
            .toPromise()
            .catch(this.handleError);
    }
    
    private post(category : Category) : Promise<Category> {
        console.log(category);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');        
        return this.authHttp
            .post(this.categoriesUrl, JSON.stringify(category), { headers: headers})
            .toPromise()
            .then(() => category)
            .catch(this.handleError);
    }
    
    private put(category : Category) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        
        let url = `${this.categoriesUrl}/${category.id}`;
        return this.authHttp
            .put(url, JSON.stringify(category), {headers : headers})
            .toPromise()
            .then(() => category)
            .catch(this.handleError);
    }
    
    private handleError(error : any) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}
