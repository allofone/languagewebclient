import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthHttp } from 'angular2-jwt/angular2-jwt';

import 'rxjs/add/operator/toPromise';

import { Question, Answer } from './question';

@Injectable()
export class QuestionService {
    constructor(private authHttp : AuthHttp){}
    
    private token : string = localStorage.getItem('id_token');
    private questionUrl = `http://localhost:8000/api/questions`;
    
    getQuestions() : Promise<Question[]> {        
        return this.authHttp.get(this.questionUrl)
            .toPromise()
            .then(response => response.json())            
            .catch(this.handleError);
    }
    
    getQuestion(id : string) : Promise<Question> {
        /*
        return this.getQuestions()
            .then(reses => reses.filter(res => res.id === id)[0]);
        */
        let url = `${this.questionUrl}/${id}`;
        return this.authHttp.get(url)
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    save(question : Question){
        if (question.id){
            return this.put(question);
        }
        return this.post(question);
    }
    
    delete(question : Question){
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let url = `${this.questionUrl}/${question.id}`;
        return this.authHttp
            .delete(url, headers)
            .toPromise()
            .catch(this.handleError);
    }
    
    private post(question : Question) : Promise<Question> {
        console.log(question);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');        
        return this.authHttp
            .post(this.questionUrl, JSON.stringify(question), { headers: headers})
            .toPromise()
            .then(() => question)
            .catch(this.handleError);
    }
    
    private put(question : Question) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        
        let url = `${this.questionUrl}/${question.id}`;
        return this.authHttp
            .put(url, JSON.stringify(question), {headers : headers})
            .toPromise()
            .then(() => question)
            .catch(this.handleError);
    }
    
    private handleError(error : any) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}