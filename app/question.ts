import { Category } from './category';

export class Question {
    id : string;
    idcategory : string;
    description : string;
    answers : Array<Answer>;
    category : Category;
}

export class Answer {
    id : string;
    description : string;
    istrue : boolean;
}