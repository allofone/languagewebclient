"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var angular2_jwt_1 = require('angular2-jwt/angular2-jwt');
var category_component_1 = require('./category.component');
var category_detail_component_1 = require('./category-detail.component');
var category_service_1 = require('./category.service');
var question_component_1 = require('./question.component');
var question_detail_component_1 = require('./question-detail.component');
var question_service_1 = require('./question.service');
var home_component_1 = require('./home.component');
var login_component_1 = require('./login.component');
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Language Learning';
        this.jwtHelper = new angular2_jwt_1.JwtHelper();
    }
    AppComponent.prototype.isLogedIn = function () {
        var token = localStorage.getItem('id_token');
        return !this.jwtHelper.isTokenExpired(token);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app/app.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [
                router_deprecated_1.ROUTER_PROVIDERS,
                category_service_1.CategoryService,
                question_service_1.QuestionService
            ]
        }),
        router_deprecated_1.RouteConfig([
            {
                path: '/home',
                name: 'Home',
                component: home_component_1.HomeComponent,
                useAsDefault: true
            },
            {
                path: '/category',
                name: 'Category',
                component: category_component_1.CategoryComponent,
            },
            {
                path: '/category-detail',
                name: 'CategoryDetail',
                component: category_detail_component_1.CategoryDetailComponent
            },
            {
                path: '/question',
                name: 'Question',
                component: question_component_1.QuestionComponent
            },
            {
                path: '/question-detail',
                name: 'QuestionDetail',
                component: question_detail_component_1.QuestionDetailComponent
            },
            {
                path: '/login',
                name: 'Login',
                component: login_component_1.LoginComponent
            }
        ]), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map