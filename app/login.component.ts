import { Component } from '@angular/core';
import { Router } from '@angular/router-deprecated';
import { JwtHelper } from 'angular2-jwt/angular2-jwt';

import { UserService } from './user.service';

@Component({
    selector : 'my-login',
    templateUrl : 'app/login.component.html',
    providers : [UserService]
})

export class LoginComponent {
    data = {
        username : '',
        password : ''
    };
    error : any;

    constructor(
        private userService : UserService,
        private router : Router
    ){}

    formSubmit() {
        this.userService
            .login(this.data.username, this.data.password)
            .then(res => {
                localStorage.setItem('id_token', res.token)
                //console.log(res.token);
                this.router.navigate(['Home']);
            })
            .catch(error => this.error = error)
    }

    logout() {
        localStorage.removeItem('id_token');
        this.router.parent.navigateByUrl('/login');
    }
}
