import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
    private token : string = '';

    constructor(private http : Http){}

    login(email:string, password:string) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        let url = `http://localhost:8000/api/authenticate`;
        let data = `email=${email}&password=${password}`;
        
        return this.http
            .post(url, data, {headers : headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    private handleError(error : any) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}