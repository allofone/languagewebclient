"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var category_1 = require('./category');
var category_service_1 = require('./category.service');
var CategoryDetailComponent = (function () {
    function CategoryDetailComponent(categoryService, routerParams) {
        this.categoryService = categoryService;
        this.routerParams = routerParams;
        this.close = new core_1.EventEmitter();
        this.navigated = false;
    }
    CategoryDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.routerParams.get('id') !== null) {
            var id = this.routerParams.get('id');
            this.navigated = true;
            this.categoryService.getCategory(id)
                .then(function (category) { return _this.category = category; });
        }
        else {
            this.navigated = false;
            this.category = new category_1.Category();
        }
    };
    CategoryDetailComponent.prototype.save = function () {
        var _this = this;
        this.categoryService
            .save(this.category)
            .then(function (category) {
            _this.category = category;
            _this.goBack(category);
        })
            .catch(function (error) { return _this.error = error; });
    };
    CategoryDetailComponent.prototype.goBack = function (savedCategory) {
        if (savedCategory === void 0) { savedCategory = null; }
        this.close.emit(savedCategory);
        if (this.navigated) {
            window.history.back();
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', category_1.Category)
    ], CategoryDetailComponent.prototype, "category", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], CategoryDetailComponent.prototype, "close", void 0);
    CategoryDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-category-detail',
            templateUrl: 'app/category-detail.component.html'
        }), 
        __metadata('design:paramtypes', [category_service_1.CategoryService, router_deprecated_1.RouteParams])
    ], CategoryDetailComponent);
    return CategoryDetailComponent;
}());
exports.CategoryDetailComponent = CategoryDetailComponent;
//# sourceMappingURL=category-detail.component.js.map