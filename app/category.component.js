"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var category_service_1 = require('./category.service');
var category_detail_component_1 = require('./category-detail.component');
var angular2_jwt_1 = require('angular2-jwt/angular2-jwt');
var CategoryComponent = (function () {
    function CategoryComponent(categoryService, router) {
        this.categoryService = categoryService;
        this.router = router;
        this.addingCategory = false;
    }
    CategoryComponent.prototype.getCategories = function () {
        var _this = this;
        this.categoryService.getCategories()
            .then(function (res) { return _this.categories = res; })
            .catch(function (error) { return _this.error = error; });
    };
    CategoryComponent.prototype.addCategory = function () {
        this.addingCategory = true;
        this.selectedCategory = null;
    };
    CategoryComponent.prototype.close = function (savedCategory) {
        this.addingCategory = false;
        if (savedCategory) {
            this.getCategories();
        }
    };
    CategoryComponent.prototype.delete = function (category, event) {
        var _this = this;
        event.stopPropagation();
        this.categoryService
            .delete(category)
            .then(function (res) {
            _this.categories = _this.categories.filter(function (c) { return c !== category; });
            if (_this.selectedCategory === category) {
                _this.selectedCategory = null;
            }
        })
            .catch(function (error) { return _this.error = error; });
    };
    CategoryComponent.prototype.ngOnInit = function () {
        this.getCategories();
    };
    CategoryComponent.prototype.onSelect = function (category) {
        this.selectedCategory = category;
        this.addingCategory = false;
    };
    CategoryComponent.prototype.goToDetail = function () {
        this.router.navigate(['CategoryDetail', { id: this.selectedCategory.id }]);
    };
    CategoryComponent = __decorate([
        core_1.Component({
            selector: 'my-category',
            templateUrl: 'app/category.component.html',
            directives: [category_detail_component_1.CategoryDetailComponent],
            providers: [category_service_1.CategoryService]
        }),
        router_deprecated_1.CanActivate(function () { return angular2_jwt_1.tokenNotExpired(); }), 
        __metadata('design:paramtypes', [category_service_1.CategoryService, router_deprecated_1.Router])
    ], CategoryComponent);
    return CategoryComponent;
}());
exports.CategoryComponent = CategoryComponent;
//# sourceMappingURL=category.component.js.map