"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var angular2_jwt_1 = require('angular2-jwt/angular2-jwt');
require('rxjs/add/operator/toPromise');
var CategoryService = (function () {
    function CategoryService(authHttp) {
        this.authHttp = authHttp;
        this.token = localStorage.getItem('id_token');
        this.categoriesUrl = "http://localhost:8000/api/categories";
    }
    CategoryService.prototype.getCategories = function () {
        return this.authHttp.get(this.categoriesUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CategoryService.prototype.getCategory = function (id) {
        return this.getCategories()
            .then(function (categories) { return categories.filter(function (category) { return category.id === id; })[0]; });
    };
    CategoryService.prototype.save = function (category) {
        if (category.id) {
            return this.put(category);
        }
        return this.post(category);
    };
    CategoryService.prototype.delete = function (category) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var url = this.categoriesUrl + "/" + category.id;
        return this.authHttp
            .delete(url, headers)
            .toPromise()
            .catch(this.handleError);
    };
    CategoryService.prototype.post = function (category) {
        console.log(category);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.authHttp
            .post(this.categoriesUrl, JSON.stringify(category), { headers: headers })
            .toPromise()
            .then(function () { return category; })
            .catch(this.handleError);
    };
    CategoryService.prototype.put = function (category) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var url = this.categoriesUrl + "/" + category.id;
        return this.authHttp
            .put(url, JSON.stringify(category), { headers: headers })
            .toPromise()
            .then(function () { return category; })
            .catch(this.handleError);
    };
    CategoryService.prototype.handleError = function (error) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    };
    CategoryService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [angular2_jwt_1.AuthHttp])
    ], CategoryService);
    return CategoryService;
}());
exports.CategoryService = CategoryService;
//# sourceMappingURL=category.service.js.map