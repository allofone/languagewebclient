import { Component } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
import { JwtHelper } from 'angular2-jwt/angular2-jwt';

import { CategoryComponent } from './category.component';
import { CategoryDetailComponent } from './category-detail.component';
import { CategoryService } from './category.service';

import { QuestionComponent } from './question.component';
import { QuestionDetailComponent } from './question-detail.component';
import { QuestionService } from './question.service';

import { HomeComponent } from './home.component';
import { LoginComponent } from './login.component';

@Component({
    selector : 'my-app',
    templateUrl : 'app/app.component.html',
    directives : [ROUTER_DIRECTIVES],
    providers : [
        ROUTER_PROVIDERS,
        CategoryService,
        QuestionService
    ]
})

@RouteConfig([    
    {
        path : '/home',
        name : 'Home',
        component : HomeComponent,
        useAsDefault : true
    },
    {
        path : '/category',
        name : 'Category',
        component : CategoryComponent,      
    },
    {
        path : '/category-detail',
        name : 'CategoryDetail',
        component : CategoryDetailComponent        
    },
    {
        path : '/question',
        name : 'Question',
        component : QuestionComponent     
    },
    {
        path : '/question-detail',
        name : 'QuestionDetail',
        component : QuestionDetailComponent        
    },
    {
        path : '/login',
        name : 'Login',
        component : LoginComponent        
    }
])

export class AppComponent {
    title = 'Language Learning';

    jwtHelper: JwtHelper = new JwtHelper();

    isLogedIn() {
        let token = localStorage.getItem('id_token');
        return !this.jwtHelper.isTokenExpired(token);
    }
}
