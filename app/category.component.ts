import { Component, OnInit } from '@angular/core';
import { Router, CanActivate } from '@angular/router-deprecated';

import { Category } from './category';
import { CategoryService } from './category.service';
import { CategoryDetailComponent } from './category-detail.component';
import { tokenNotExpired } from 'angular2-jwt/angular2-jwt';

@Component({
    selector : 'my-category',
    templateUrl : 'app/category.component.html',
    directives: [CategoryDetailComponent],
    providers: [CategoryService]
})

@CanActivate(() => tokenNotExpired())
export class CategoryComponent implements OnInit {
    categories : Category[];
    error : any;
        
    selectedCategory : Category;
    addingCategory = false;
    
    constructor(
        private categoryService: CategoryService,
        private router : Router
    ){}
    
    getCategories() {
        this.categoryService.getCategories()
            .then(res => this.categories = res)
            .catch(error => this.error = error);
    }
    
    addCategory() {
        this.addingCategory = true;
        this.selectedCategory = null;
    }
    
    close(savedCategory : Category) {
        this.addingCategory = false;
        if (savedCategory) { this.getCategories(); }
    }
    
    delete(category : Category, event : any){
        event.stopPropagation();
        this.categoryService
            .delete(category)
            .then(res => {
                this.categories = this.categories.filter(c => c !== category);
                if (this.selectedCategory === category) { this.selectedCategory = null; }
            })
            .catch(error => this.error = error);
    }
    
    ngOnInit() {
        this.getCategories();
    }
    
    onSelect(category: Category) { 
        this.selectedCategory = category; 
        this.addingCategory = false;
    }
    
    goToDetail(){
        this.router.navigate(['CategoryDetail', { id: this.selectedCategory.id }])
    }
}
