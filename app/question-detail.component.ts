import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';
import { Question } from './question';
import { QuestionService } from './question.service';
import { Category } from './category';
import { CategoryService } from './category.service';

@Component({
    selector : 'my-question-detail',
    templateUrl : 'app/question-detail.component.html'
})
export class QuestionDetailComponent {
    @Input() question : Question;
    @Output() close = new EventEmitter();
    error : any;
    navigated = false;

    categories : Category[];
    
    constructor (
        private categoryService : CategoryService,
        private questionService : QuestionService,
        private routerParams : RouteParams
    ){}

    getCategories() {
        this.categoryService.getCategories()
            .then(res => this.categories = res)
            .catch(error => this.error = error);
    }

    getQuestion() {
        console.log(this.question);
    }
    
    ngOnInit() {
        if (this.routerParams.get('id') !== null){
            let id = this.routerParams.get('id');
            this.navigated = true;
            this.questionService.getQuestion(id)
                .then(question => this.question = question);            
        }else {
            this.navigated = false;
            this.question = new Question();
        }
        this.getCategories();
    }
    
    save() {
        console.log(this.question);
        this.questionService
            .save(this.question)
            .then(question => {
                this.question = question;
                this.goBack(question);
            })
            .catch(error => this.error = error);
    }
    
    goBack(savedQuestion : Question = null){
        this.close.emit(savedQuestion);
        if (this.navigated){
            window.history.back();
        }
    }
}