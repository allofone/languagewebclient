"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var question_1 = require('./question');
var question_service_1 = require('./question.service');
var category_service_1 = require('./category.service');
var QuestionDetailComponent = (function () {
    function QuestionDetailComponent(categoryService, questionService, routerParams) {
        this.categoryService = categoryService;
        this.questionService = questionService;
        this.routerParams = routerParams;
        this.close = new core_1.EventEmitter();
        this.navigated = false;
    }
    QuestionDetailComponent.prototype.getCategories = function () {
        var _this = this;
        this.categoryService.getCategories()
            .then(function (res) { return _this.categories = res; })
            .catch(function (error) { return _this.error = error; });
    };
    QuestionDetailComponent.prototype.getQuestion = function () {
        console.log(this.question);
    };
    QuestionDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.routerParams.get('id') !== null) {
            var id = this.routerParams.get('id');
            this.navigated = true;
            this.questionService.getQuestion(id)
                .then(function (question) { return _this.question = question; });
        }
        else {
            this.navigated = false;
            this.question = new question_1.Question();
        }
        this.getCategories();
    };
    QuestionDetailComponent.prototype.save = function () {
        var _this = this;
        console.log(this.question);
        this.questionService
            .save(this.question)
            .then(function (question) {
            _this.question = question;
            _this.goBack(question);
        })
            .catch(function (error) { return _this.error = error; });
    };
    QuestionDetailComponent.prototype.goBack = function (savedQuestion) {
        if (savedQuestion === void 0) { savedQuestion = null; }
        this.close.emit(savedQuestion);
        if (this.navigated) {
            window.history.back();
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', question_1.Question)
    ], QuestionDetailComponent.prototype, "question", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], QuestionDetailComponent.prototype, "close", void 0);
    QuestionDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-question-detail',
            templateUrl: 'app/question-detail.component.html'
        }), 
        __metadata('design:paramtypes', [category_service_1.CategoryService, question_service_1.QuestionService, router_deprecated_1.RouteParams])
    ], QuestionDetailComponent);
    return QuestionDetailComponent;
}());
exports.QuestionDetailComponent = QuestionDetailComponent;
//# sourceMappingURL=question-detail.component.js.map