"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var question_service_1 = require('./question.service');
var question_detail_component_1 = require('./question-detail.component');
var angular2_jwt_1 = require('angular2-jwt/angular2-jwt');
var QuestionComponent = (function () {
    function QuestionComponent(questionService, router) {
        this.questionService = questionService;
        this.router = router;
        this.addingQuestion = false;
    }
    QuestionComponent.prototype.getQuestions = function () {
        var _this = this;
        this.questionService.getQuestions()
            .then(function (res) { return _this.questions = res; })
            .catch(function (error) { return _this.error = error; });
    };
    QuestionComponent.prototype.addQuestion = function () {
        this.addingQuestion = true;
        this.selectedQuestion = null;
    };
    QuestionComponent.prototype.close = function (savedQuestion) {
        this.addingQuestion = false;
        if (savedQuestion) {
            this.getQuestions();
        }
    };
    QuestionComponent.prototype.ngOnInit = function () {
        this.getQuestions();
    };
    QuestionComponent.prototype.onSelect = function (question) {
        this.selectedQuestion = question;
        this.addingQuestion = false;
    };
    QuestionComponent.prototype.goToDetail = function () {
        this.router.navigate(['QuestionDetail', { id: this.selectedQuestion.id }]);
    };
    QuestionComponent = __decorate([
        core_1.Component({
            selector: 'my-question',
            templateUrl: 'app/question.component.html',
            directives: [question_detail_component_1.QuestionDetailComponent],
            providers: [question_service_1.QuestionService]
        }),
        router_deprecated_1.CanActivate(function () { return angular2_jwt_1.tokenNotExpired(); }), 
        __metadata('design:paramtypes', [question_service_1.QuestionService, router_deprecated_1.Router])
    ], QuestionComponent);
    return QuestionComponent;
}());
exports.QuestionComponent = QuestionComponent;
//# sourceMappingURL=question.component.js.map