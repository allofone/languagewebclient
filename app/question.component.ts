import { Component, OnInit } from '@angular/core';
import { Router, CanActivate } from '@angular/router-deprecated';

import { Question } from './question';
import { QuestionService } from './question.service';
import { QuestionDetailComponent } from './question-detail.component';
import { tokenNotExpired } from 'angular2-jwt/angular2-jwt';

@Component({
    selector : 'my-question',
    templateUrl : 'app/question.component.html',
    directives : [QuestionDetailComponent],
    providers : [QuestionService]    
})

@CanActivate(() => tokenNotExpired())
export class QuestionComponent implements OnInit {
    questions : Question[];
    error : any;

    selectedQuestion : Question;
    addingQuestion = false;

    constructor(
        private questionService : QuestionService,
        private router : Router
    ){}

    getQuestions() {
        this.questionService.getQuestions()
            .then(res => this.questions = res)
            .catch(error => this.error = error);
    }

    addQuestion() {
        this.addingQuestion = true;
        this.selectedQuestion = null;
    }
    
    close(savedQuestion : Question) {
        this.addingQuestion = false;
        if (savedQuestion) { this.getQuestions(); }
    }

    ngOnInit() {
        this.getQuestions();
    }

    onSelect(question: Question) { 
        this.selectedQuestion = question; 
        this.addingQuestion = false;
    }
    
    goToDetail(){
        this.router.navigate(['QuestionDetail', { id: this.selectedQuestion.id }])
    }
}

